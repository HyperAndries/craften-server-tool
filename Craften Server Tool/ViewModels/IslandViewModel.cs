﻿using Caliburn.Micro;
using Craften_Server_Tool.Api.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Craften_Server_Tool.ViewModels {
    public class IslandViewModel : PropertyChangedBase {
        private Island _data;
        public Island Data {
            get { return _data; }
            set {
                _data = value;
                NotifyOfPropertyChange(() => Data);
                NotifyOfPropertyChange(() => Members);
            }
        }

        public List<String> Members {
            get {
                var l = new List<String>();
                if (Data != null) {
                    l.Add(Data.Leader);
                    l.AddRange(Data.Party);
                }
                return l;
            }
        }

        public IslandViewModel(Island data) {
            Data = data;
        }
    }
}
