﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Craften_Server_Tool.Api;
using Craften_Server_Tool.Api.Server;

namespace Craften_Server_Tool.ViewModels {
    public class ServerStatusViewModel : PropertyChangedBase {
        private ServerStatus _status;

        public ServerStatus Status {
            get { return _status; }
            set {
                _status = value;
                NotifyOfPropertyChange(() => Status);
            }
        }

        public ServerStatusViewModel() {
            Update();
        }

        private async void Update() {
            try {
                Status = await CraftenApi.GetServerStatus();
            }
            catch (ApiException) {
                //TODO Display error message
            }
        }
    }
}
