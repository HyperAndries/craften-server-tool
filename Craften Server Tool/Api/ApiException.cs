﻿using System;

namespace Craften_Server_Tool.Api {
    public class ApiException : Exception {
        public ApiException(String message, Exception innerException)
            : base(message, innerException) {
        }

        public ApiException(string message)
            : base(message) {
        }
    }
}
