﻿using System;
using System.Threading.Tasks;
using Craften_Server_Tool.Api.Player;
using Craften_Server_Tool.Api.Server;
using RestSharp;

namespace Craften_Server_Tool.Api {
    public static partial class CraftenApi {
        public static async Task<PlayerInfo> GetPlayer(String nickname) {
            var request = new RestRequest("server/players/{nickname}");
            request.AddUrlSegment("nickname", nickname);

            var player = await ExecuteAsync<PlayerInfo>(request);
            if (player == null)
                throw new ApiException("Could not load player.");
            return player;
        }

        public static async Task<ServerStatus> GetServerStatus() {
            var request = new RestRequest("server/status");

            var status = await ExecuteAsync<ServerStatus>(request);
            if (status == null)
                throw new ApiException("Could not get status.");
            return status;
        }
    }
}
