﻿using System;
using System.Collections.Generic;

namespace Craften_Server_Tool.Api.Player {
    public class Plot {
        public PlotId Id { get; set; }
        public String Owner { get; set; }
        public List<String> Helpers { get; set; }
    }
}
