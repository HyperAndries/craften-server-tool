﻿using System;

namespace Craften_Server_Tool.Api.Player {
    public class PlotId {
        public int X { get; set; }
        public int Z { get; set; }
        public String World { get; set; }
    }
}
