﻿namespace Craften_Server_Tool.Api.Player {
    public class McMmoInfo {
        public McMmoSkills Skills { get; set; }
        public int PowerLevel { get; set; }
        public int Rank { get; set; }
    }
}
